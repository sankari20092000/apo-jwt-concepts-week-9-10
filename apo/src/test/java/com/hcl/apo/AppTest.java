package com.hcl.apo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.hcl.apo.dto.EmployeeDTO;

/**
 * Unit test for simple App.
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.hcl.apo")
public class AppTest 
{
	public  static void main(String args[])
	{
		ApplicationContext context=new AnnotationConfigApplicationContext("AppTest.class");
	
	EmployeeManager manager=context.getBean(EmployeeManager.class);
	
	manager.getEmployeeById(101);
	manager.createEmployee(new EmployeeDTO());
	manager.deleteEmployee(111);
	manager.updateEmployee(new EmployeeDTO());
	}
}
